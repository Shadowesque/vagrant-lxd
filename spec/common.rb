require 'vagrant'
require 'rspec/its'
require 'rspec/mocks'

module Kernel
  def silently
    _stdout, _stderr = $stdout, $stderr
    $stdout, $stderr = StringIO.new, StringIO.new
    yield if block_given?
  ensure
    $stdout, $stderr = _stdout, _stderr
  end
end

RSpec.configure do |config|
  config.expect_with(:rspec) { |c| c.syntax = [:should, :expect] }
  config.mock_with(:rspec) { |c| c.syntax = [:should, :expect] }
end
